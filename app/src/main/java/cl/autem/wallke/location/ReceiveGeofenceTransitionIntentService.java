package cl.autem.wallke.location;

public class ReceiveGeofenceTransitionIntentService{}
/*

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;

*/
/**
 * This class receives geofence transition events from Location Services, in the
 * form of an Intent containing the transition type and geofence id(s) that triggered
 * the event.
 *//*

public class ReceiveGeofenceTransitionIntentService extends IntentService {
    */
/**
     * Sets an identifier for the service
     *//*

    public ReceiveGeofenceTransitionIntentService() {
        super("ReceiveGeofenceTransitionsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        // Create a local broadcast Intent
        Intent broadcastIntent = new Intent();

        // Give it the category for all intents sent by the Intent Service
        broadcastIntent.addCategory(GeofenceUtils.CATEGORY_LOCATION_SERVICES);


        // First check for errors
        if (LocationClient.hasError(intent)) {
            // Get the error code with a static method
            int errorCode = LocationClient.getErrorCode(intent);
        } else {
            // Get the type of transition (entry or exit)
            int transition =
                    LocationClient.getGeofenceTransition(intent);

            if ((transition == Geofence.GEOFENCE_TRANSITION_ENTER) ||
                    (transition == Geofence.GEOFENCE_TRANSITION_EXIT)) {

                // Post a notification
            } else {
                // handle the error
            }
        }
    }
}*/
