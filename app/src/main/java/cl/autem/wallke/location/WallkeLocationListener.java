package cl.autem.wallke.location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;

/**
 * Created by Alejandro on 07-04-2015.
 */
public class WallkeLocationListener implements LocationListener {

    private WallkeLocationManager locationManager;

    public WallkeLocationListener(WallkeLocationManager locationManager){
        this.locationManager = locationManager;
    }
    @Override
    public void onLocationChanged(Location location) {
        locationManager.useNewLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
