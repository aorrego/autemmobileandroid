package cl.autem.wallke.location;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import cl.autem.wallke.R;
import cl.autem.wallke.app.MainMapActivity;
import cl.autem.wallke.app.WallkeApplication;
import cl.autem.wallke.app.geofencing.GeofenceManager;
import cl.autem.wallke.app.tasks.GetPlacesTask;
import cl.autem.wallke.app.ui.HomeMapViewManager;
import cl.autem.wallke.app.ui.Models.MapPinItem;
import cl.autem.wallke.app.ui.Renders.MapClusterRender;
import cl.autem.wallke.services.place.IPlaceManager;
import cl.autem.wallke.services.place.Place;

public class WallkeLocationManager {


    private MainMapActivity parentActivity;
    private GoogleMap map;
    private ClusterManager<MapPinItem> mClusterManager;
    private WallkeLocationListener locationListener;
    private boolean firstCenterTime = true;
    private MapPinItem clickedClusterItem;
    private IPlaceManager placeManager;
    private HomeMapViewManager homeMapViewManager;
    private GeofenceManager geofenceManager;
    private GoogleApiClient googleApiClient;

    public WallkeLocationManager( MainMapActivity parentActivity,
                                  IPlaceManager placeManager,
                                  HomeMapViewManager homeMapViewManager,
                                  GeofenceManager geofenceManager, GoogleApiClient googleApiClient){
        this.parentActivity = parentActivity;
        this.placeManager = placeManager;
        this.homeMapViewManager = homeMapViewManager;
        this.geofenceManager = geofenceManager;
        this.googleApiClient = googleApiClient;
    }

    public void initialize(){
        locationListener = new WallkeLocationListener(this);

        initLocationManager((LocationManager) parentActivity.getSystemService(Context.LOCATION_SERVICE));
        map = initMap();
        initClusterManager();
    }

    private void initLocationManager(LocationManager locationManagerIn) {
        LocationManager locationManager = locationManagerIn;
        // Create a criteria object needed to retrieve the provider
        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);

        // Get the name of the best available provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Define a listener that responds to location updates
        // request that the provider send this activity GPS updates every 20 seconds
        locationManager.requestLocationUpdates(provider, 60000, 20, getLocationListener());
    }

    private GoogleMap initMap() {
        SupportMapFragment sf = ((SupportMapFragment) parentActivity.getSupportFragmentManager().findFragmentById(R.id.map));

        map = sf.getMap();
        if (map == null) {
            Toast.makeText(parentActivity, "Google Maps no disponible", Toast.LENGTH_LONG).show();
        }
        map.animateCamera(CameraUpdateFactory.zoomTo(16));
        map.setMyLocationEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(false);
        map.setIndoorEnabled(true);
        return map;
    }

    private void initClusterManager() {
        mClusterManager = new ClusterManager<MapPinItem>( parentActivity.getApplicationContext(), map);
        map.setOnInfoWindowClickListener(mClusterManager);
        map.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        map.setOnMarkerClickListener(mClusterManager);
        map.setOnCameraChangeListener(mClusterManager);

        //Hide description panel On Map click
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (homeMapViewManager.getDescriptionPanel().getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    homeMapViewManager.getDescriptionPanel().setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    homeMapViewManager.getFabButton().showFloatingActionButton();
                }
            }
        });

        //final MapPinItem[] clickedItem = {null};
        mClusterManager
                .setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MapPinItem>() {
                    @Override
                    public boolean onClusterItemClick(MapPinItem mapPinItem) {
                        //clickedItem[0] = mapPinItem;

                        MyCustomAdapterForItems ca = new MyCustomAdapterForItems(parentActivity);

                        if (clickedClusterItem != null)
                            ca.setClickedClusterItem(clickedClusterItem);
                        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(ca);
                        return drawPlaceResume(mapPinItem);
            }
        });



        mClusterManager.cluster();
    }

    private boolean drawPlaceResume(MapPinItem mapPinItem) {
        clickedClusterItem = mapPinItem;
        Log.d("INFO", "asd");
        Place place = null;
        //Add marker info info al marker
        for (Place placeIterator :  ((WallkeApplication) parentActivity.getApplication()).getPlaces()) {
            if (placeIterator._id.equals(mapPinItem.id)) {
                place = placeIterator;
            }
        }

        if (place != null) {
            //Show detail view
            homeMapViewManager.getFabButton().hideFloatingActionButton();
            homeMapViewManager.getDescriptionPanel().setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            //Set description
            ((TextView) parentActivity.findViewById(R.id.descriptionTxt)).setText(place.description);
            ((ImageView) parentActivity.findViewById(R.id.detailImg)).setImageDrawable(placeManager.getImage(place));
            //TODO Get wallkers in place
            ((TextView) parentActivity.findViewById(R.id.wallkersNumber)).setText("14 Wallkers now");
        }

        return false;
    }

    public void useNewLocation(Location location) {
        if (firstCenterTime) {
            CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
            map.moveCamera(center);
            firstCenterTime = false;
        }

        //TODO get surrounding places/events/users.
        drawNearPlaces(location);
    }

    private void drawNearPlaces(Location location) {

        //TODO cambiar ZoomLevel
        Location loc = new Location(location);
        mClusterManager.clearItems();

        GetPlacesTask placesTask = new GetPlacesTask(parentActivity,location, new String[]{}, 16, this, homeMapViewManager);

        placesTask.execute();

    }
    public void createCluster(Place[] places){
        //places = new PlaceServices().getHomePlaces(loc,new String[]{},16);
        MapPinItem mapPinItem;
        if (places != null) {
            for (Place place : places) {
                //drawMarkerWithCircle(new LatLng( place.location.loc[0], place.location.loc[1]  ));
                mapPinItem = new MapPinItem(place.location.loc[0], place.location.loc[1], place.name,"", place._id);

                if (place.categories.length != 0){
                    mapPinItem.category = place.categories[0].name;
                }
                mClusterManager.addItem(mapPinItem);
            }
        }

        mClusterManager.setRenderer(new MapClusterRender<MapPinItem>(parentActivity.getApplicationContext(), map, mClusterManager));
        mClusterManager.cluster();


        geofenceManager.initGeofences();
    }

    private Circle mCircle;
    private Marker mMarker;
    private GoogleMap mGoogleMap;


    private void drawMarkerWithCircle(LatLng position){
        double radiusInMeters = 10.0;
        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill

        CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        mCircle = mGoogleMap.addCircle(circleOptions);

        MarkerOptions markerOptions = new MarkerOptions().position(position);
        mMarker = mGoogleMap.addMarker(markerOptions);
    }

    public LocationListener getLocationListener() {
        return locationListener;
    }

    /*public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }*/
}
