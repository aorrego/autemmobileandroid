package cl.autem.wallke.location;

import android.app.Notification;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import cl.autem.wallke.R;
import cl.autem.wallke.app.ui.Models.MapPinItem;

public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

    private ActionBarActivity parentActivity;
    private MapPinItem clickedClusterItem;

    public MyCustomAdapterForItems(ActionBarActivity parentActivity){
        this.parentActivity = parentActivity;
    }

    public void setClickedClusterItem(MapPinItem itemClicked){
        this.clickedClusterItem = itemClicked;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        // TODO Auto-generated method stub
        View myContentsView = parentActivity.getLayoutInflater().inflate(R.layout.info_window_layout, null);
        if (clickedClusterItem != null) {
            ((TextView) myContentsView.findViewById(R.id.tvTitle)).setText(clickedClusterItem.title);
            ((TextView) myContentsView.findViewById(R.id.tvSnippet)).setText(clickedClusterItem.category);

        }
        return myContentsView;
    }
}
