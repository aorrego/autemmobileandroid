package cl.autem.wallke.location;

import android.util.Log;

import com.google.android.gms.location.Geofence;

import java.util.List;

import cl.autem.wallke.services.event.Event;

/**
 * Created by Aorrego on 26-06-2014.
 */
public class GeofenceFactory {
    public void initGeofencesFromEvents(Event[] events) {
        Log.d("onCreate", "inicializarGeofences");

        List<Geofence> geofences = null;
        try {
            for (int i = 0; i < events.length; i++) {
                SimpleGeofence geofence =
                        new SimpleGeofence(events[i].get_Id(),
                                events[i].getLat(),
                                events[i].getLng(), Float.parseFloat(events[i].getRad()),
                                Constants.GEOFENCE_EXPIRATION_IN_HOURS,
                                Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT);
                geofences.add(geofence.toGeofence());
            }

        } catch (Exception ex) {
            String err = ex.toString();
            Log.d("initGeofencesFromEvents", err);
        }
    }


}
