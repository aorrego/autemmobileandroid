package cl.autem.wallke.location;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.GeofencingRequest;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

import cl.autem.wallke.R;
import cl.autem.wallke.app.ui.Models.MapPinItem;
import cl.autem.wallke.app.ui.Renders.MapClusterRender;
import cl.autem.wallke.services.event.Event;
import cl.autem.wallke.services.place.Place;

public class LocationFactory {

    /*private Context applicationContext;
    private GoogleMap map; // Might be null if Google Play services APK is not available.
    private FragmentActivity parentActivity;
    private GoogleApiClient mLocationClient;
    private GeofenceUtils.REQUEST_TYPE requestType;
    private SimpleGeofenceStore database;
    private SimpleGeofence geofence;
    private List<Geofence> geofences;
    private boolean inProgress;
    private PendingIntent transitionPendingIntent;
    private Event[] allEvents;
    private boolean firstCenterTime = true;
    public ClusterManager<MapPinItem> mClusterManager;
    public Place[] places;

    public LocationFactory(FragmentActivity activity, GoogleApiClient googleApiClient, Context applicationContext) {
        geofences = new ArrayList<Geofence>();
        this.applicationContext = applicationContext;
        this.setmLocationClient(googleApiClient);
        parentActivity = activity;
    }

    public void addGeofences() {
        Log.d("addGeofences", "addGeofences");
        // Start a request to add geofences
        requestType = GeofenceUtils.REQUEST_TYPE.ADD;
        *//*
         * Test for Google Play services after setting the request type.
         * If Google Play services isn't present, the proper request
         * can be restarted.
         *//*
        if (!servicesConnected(parentActivity)) {
            return;
        }
        *//*
         * Create a new location client object. Since the current
         * activity class implements ConnectionCallbacks and
         * OnConnectionFailedListener, pass the current activity object
         * as the listener for both parameters
         *//*
        //mLocationClient = new LocationClient(parentActivity, parentActivity, parentActivity);
        // If a request is not already underway
        if (!isInProgress()) {
            // Indicate that a request is underway
            setInProgress(true);
            // Request a connection from the client to Location Services
            getLocationClient().connect();
        } else {
            *//*
             * A request is already underway. You can handle
             * this situation by disconnecting the client,
             * re-setting the flag, and then re-trying the
             * request.
             *//*
        }
    }

    public void setGeofences(Place[] places){
        Log.d("onCreate", "inicializarGeofences");

        try {
            database = new SimpleGeofenceStore(parentActivity);

            for (int i = 0; i < places.length; i++) {

                geofences.add( new Geofence.Builder()
                        // Set the request ID of the geofence. This is a string to identify this
                        // geofence.
                        .setRequestId(places[i]._id)
                        .setCircularRegion(
                                places[i].location.loc[0],
                                places[i].location.loc[1],
                                100
                        )
                        .setExpirationDuration(Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                                Geofence.GEOFENCE_TRANSITION_EXIT)
                        .build()  );
            }

            getLocationClient().connect();
        } catch (Exception ex) {
            String err = ex.toString();
            Log.d("inicializarGeofences", err);
        }
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(geofences);
        return builder.build();
    }

   *//* private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }
*//*

    public void inicializarGeofences(Place[] places) {
        Log.d("onCreate", "inicializarGeofences");

        try {
            database = new SimpleGeofenceStore(parentActivity);

            for (int i = 0; i < places.length; i++) {
                SimpleGeofence geofence =
                        new SimpleGeofence(places[i]._id,
                                places[i].location.loc[0],
                                places[i].location.loc[1], Float.parseFloat( "100"   ),
                                Constants.GEOFENCE_EXPIRATION_IN_HOURS,
                                Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT);
                database.setGeofence(places[i]._id, geofence);
                geofences.add(geofence.toGeofence());
            }

            getLocationClient().connect();
        } catch (Exception ex) {
            String err = ex.toString();
            Log.d("inicializarGeofences", err);
        }
    }

    public void inicializarMapa() {
        SupportMapFragment sf = ((SupportMapFragment) parentActivity.getSupportFragmentManager().findFragmentById(R.id.map));
        setMap(sf.getMap());
        if (getMap() == null) {
            Toast.makeText(parentActivity, "Google Maps no disponible", Toast.LENGTH_LONG).show();
        }

        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);
        getMap().animateCamera(zoom);
        getMap().setMyLocationEnabled(true);
        mClusterManager = new ClusterManager<MapPinItem>(applicationContext,getMap());
    }

    public boolean servicesConnected(Activity activity) {
        int resultCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, activity, 0);
            if (dialog != null) {
                Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                        resultCode,
                        activity,
                        Constants.CONNECTION_FAILURE_RESOLUTION_REQUEST);
                errorDialog.show();
            }
            return false;
        }
    }

    public void inicializarLocationManager(LocationManager locationManagerIn) {
        LocationManager locationManager = locationManagerIn;
        // Create a criteria object needed to retrieve the provider
        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);

        // Get the name of the best available provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                usarNuevaLocalidad(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
        // request that the provider send this activity GPS updates every 20 seconds
        locationManager.requestLocationUpdates(provider, 60000, 20, locationListener);
        getMap().setIndoorEnabled(true);
    }

    public void usarNuevaLocalidad(Location location) {
        if (firstCenterTime) {
            CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
            getMap().moveCamera(center);
            firstCenterTime = false;
        }

        //TODO get surrounding places/events/users.
        setUpClusterer(location);
    }

    public void createGeofence(double latitude, double longitude, int radius,
                               String geofenceType, String title) {

        Marker stopMarker = getMap().addMarker(new MarkerOptions()
                .draggable(true)
                .position(new LatLng(latitude, longitude))
                .title(title)
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.ic_launcher)));

        getMap().addCircle(new CircleOptions()
                .center(new LatLng(latitude, longitude)).radius(radius)
                .fillColor(Color.parseColor("#B2A9F6")));
    }

    public GoogleMap getMap() {
        return map;
    }

    public void setMap(GoogleMap map) {
        this.map = map;
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }

    private PendingIntent createRequestPendingIntent() {

        if (null != transitionPendingIntent) {

            return transitionPendingIntent;

        } else {
            Intent intent = new Intent("com.example.ACTION_RECEIVE_GEOFENCE");
            System.out.println("Intent" + intent);

            return PendingIntent.getBroadcast(
                    applicationContext,
                    0,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }
    }

    public PendingIntent getTransitionPendingIntent() {
        if (this.transitionPendingIntent == null )
            this.transitionPendingIntent = createRequestPendingIntent();
        return this.transitionPendingIntent;
    }

    public void setTransitionPendingIntent(PendingIntent transitionPendingIntent) {
        this.transitionPendingIntent = transitionPendingIntent;
    }

    public SimpleGeofence getGeofence() {
        return geofence;
    }

    public void setGeofence(SimpleGeofence geofence) {
        this.geofence = geofence;
    }

    public List<Geofence> getGeofences() {
        return geofences;
    }

    public void setGeofences(List<Geofence> geofences) {
        this.geofences = geofences;
    }

    public GoogleApiClient getLocationClient() {
        return mLocationClient;
    }

    public void setmLocationClient(GoogleApiClient locationClient) {
        this.mLocationClient = locationClient;
    }

    public void paintAllEvents(Event[] events) {
        if( events != null && events.length > 0 ) {

            for ( Event event : events)
            {
                createGeofence(event.getLat(), event.getLng(), Integer.parseInt(event.getRad()),  "", event.getName() );
            }
        }
    }

    private void setUpClusterer(Location location) {

        //TODO cambiar ZoomLevel
        Location loc = new Location(location);
        mClusterManager.clearItems();
        places = new PlaceServices().getHomePlaces(loc,new String[]{},16);
        MapPinItem mapPinItem;
        if (places != null) {
            for (Place place : places) {
                mapPinItem = new MapPinItem(place.location.loc[0], place.location.loc[1], place.name,"", place._id);
                if (place.categories.length != 0){
                    mapPinItem.category = place.categories[0].name;
                }
                mClusterManager.addItem(mapPinItem);
            }
        }
        mClusterManager.setRenderer(new MapClusterRender<MapPinItem>(applicationContext, getMap(), mClusterManager));
        mClusterManager.cluster();
    }*/
}
