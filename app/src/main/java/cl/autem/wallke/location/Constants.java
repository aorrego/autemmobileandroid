package cl.autem.wallke.location;

import android.text.format.DateUtils;

/**
 * Created by Developer on 23/04/2014.
 */
public class Constants {
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    public static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = GEOFENCE_EXPIRATION_IN_HOURS * DateUtils.HOUR_IN_MILLIS;
}
