package cl.autem.wallke.app.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

import cl.autem.wallke.R;
import cl.autem.wallke.app.MainMapActivity;
import cl.autem.wallke.app.TimePickerFragment;
import cl.autem.wallke.app.WallkeApplication;
import cl.autem.wallke.app.messaging.CommunicationManager;
import cl.autem.wallke.app.ui.Models.MapPinItem;
import cl.autem.wallke.app.ui.Models.NavItem;
import cl.autem.wallke.services.place.IPlaceManager;
import cl.autem.wallke.services.place.Place;

public class HomeMapViewManager {

    private ActionBarActivity parentActivity;
    private DrawerLayout mDrawerLayout;
    private FloatingActionButton fabButton;
    private ListView mDrawerList;
    private RelativeLayout mDrawerPane;
    private ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    private ActionBarDrawerToggle mDrawerToggle;
    private SlidingUpPanelLayout descriptionPanel;
    private MapPinItem clickedClusterItem;
    private cl.autem.wallke.app.ui.dialog.ErrorDialogFragment errorDialogFragment;
    private CommunicationManager communicationManager;
    private IPlaceManager placeManager;
    private ProgressDialog ringProgressDialog = null;

    public HomeMapViewManager(ActionBarActivity parentActivity, CommunicationManager communicationManager, IPlaceManager placeManager){
        this.placeManager = placeManager;
        this.communicationManager = communicationManager;
        this.parentActivity = parentActivity;
        this.errorDialogFragment = new cl.autem.wallke.app.ui.dialog.ErrorDialogFragment();
    }

    public void initialize(){
        initDrawer();
        loadUserProfileImage();
        initFAB();
        initDescriptionPanel();
    }

    public boolean drawPlaceResume(MapPinItem mapPinItem) {
        clickedClusterItem = mapPinItem;
        Log.d("INFO", "asd");
        Place place = null;
        //Add marker info info al marker
        for (Place placeIterator : ((WallkeApplication) parentActivity.getApplication()).getPlaces()) {
            if (placeIterator._id.equals(mapPinItem.id)) {
                place = placeIterator;
            }
        }

        if (place != null) {
            //Show detail view
            getFabButton().hideFloatingActionButton();
            getDescriptionPanel().setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            //Set description
            ((TextView) parentActivity.findViewById(R.id.descriptionTxt)).setText(place.description);
            ((ImageView) parentActivity.findViewById(R.id.detailImg)).setImageDrawable(placeManager.getImage(place));
            //TODO Get wallkers in place
            ((TextView) parentActivity.findViewById(R.id.wallkersNumber)).setText("14 Wallkers now");
        }

        return false;
    }

    public void showProgressDialog(){
         ringProgressDialog = ProgressDialog.show(parentActivity, "Please wait ...", "Loading places nearby ...", true);
         ringProgressDialog.setCancelable(true);

    }

    public void hideProgressDialog(){
        ringProgressDialog.dismiss();
    }

    private void loadUserProfileImage() {
        int loader = R.drawable.loader;

        ImageView image = (ImageView) parentActivity.findViewById(R.id.avatar);

        SharedPreferences settings = parentActivity.getSharedPreferences("Usuario", 0);

        // Image url
        String image_url = settings.getString("userImageUrl", "nada"); //http://api.androidhive.info/images/sample.jpg";

        // ImageLoader class instance
        ImageLoader imgLoader = new ImageLoader(parentActivity.getApplicationContext());

        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView
        imgLoader.DisplayImage(image_url, loader, image);

      /*  final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/
    }

    private void initDrawer() {
        mNavItems.add(new NavItem(parentActivity.getString(R.string.drawer_title1), R.drawable.ic_drawer_profile));
        mNavItems.add(new NavItem(parentActivity.getString(R.string.drawer_title2), R.drawable.ic_drawer_home));
        mNavItems.add(new NavItem(parentActivity.getString(R.string.drawer_title3), R.drawable.ic_drawer_calendar));
        mNavItems.add(new NavItem(parentActivity.getString(R.string.drawer_title4), R.drawable.ic_drawer_notification));
        mNavItems.add(new NavItem(parentActivity.getString(R.string.drawer_title5), R.drawable.ic_drawer_exit));

        // DrawerLayout
        mDrawerLayout = (DrawerLayout) parentActivity.findViewById(R.id.drawer_layout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) parentActivity.findViewById(R.id.drawerPane);
        mDrawerList = (ListView) parentActivity.findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(parentActivity, mNavItems);
        mDrawerList.setAdapter(adapter);
        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }
        });
        parentActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(parentActivity, mDrawerLayout,R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                parentActivity.invalidateOptionsMenu();
                getFabButton().hideFloatingActionButton();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                parentActivity.invalidateOptionsMenu();
                getFabButton().showFloatingActionButton();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void initDescriptionPanel() {
        setDescriptionPanel((SlidingUpPanelLayout) parentActivity.findViewById(R.id.sliding_layout));
        getDescriptionPanel().setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelExpanded(View panel) {
                getFabButton().hideFloatingActionButton();
            }

            @Override
            public void onPanelCollapsed(View panel) {
                getFabButton().showFloatingActionButton();
            }

            @Override
            public void onPanelAnchored(View panel) {
            }

            @Override
            public void onPanelHidden(View panel) {
            }
        });
        getDescriptionPanel().setTouchEnabled(false);
        getDescriptionPanel().setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    private void initFAB() {
        setFabButton(new FloatingActionButton.Builder(parentActivity)
                .withDrawable(parentActivity.getResources().getDrawable(R.drawable.ic_fab_add))
                .withButtonColor(parentActivity.getResources().getColor(R.color.WallkeGreen))
                .withGravity(Gravity.BOTTOM | Gravity.RIGHT)
                .withMargins(0, 0, 16, 16)
                .create());
        getFabButton().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText editText = (EditText) parentActivity.findViewById(R.id.textComoEsta);
                String text = editText.getText().toString();

                communicationManager.sendMessage(text);
            }
        });
    }

    /*
    * Called when a particular item from the navigation drawer
    * is selected.
    * */
    private void selectItemFromDrawer(int position) {
        Fragment fragment = new TimePickerFragment();

        FragmentManager fragmentManager = parentActivity.getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.map, fragment)
                .commit();

        mDrawerList.setItemChecked(position, true);
        parentActivity.setTitle(mNavItems.get(position).mTitle);

        // Close the drawer
        mDrawerLayout.closeDrawer(mDrawerPane);
    }

    public void showErrorDialog(int errorCode) {
        errorDialogFragment.showErrorDialog(errorCode);
    }

    public FloatingActionButton getFabButton() {
        return fabButton;
    }

    public void setFabButton(FloatingActionButton fabButton) {
        this.fabButton = fabButton;
    }

    public SlidingUpPanelLayout getDescriptionPanel() {
        return descriptionPanel;
    }

    public void setDescriptionPanel(SlidingUpPanelLayout descriptionPanel) {
        this.descriptionPanel = descriptionPanel;
    }
}
