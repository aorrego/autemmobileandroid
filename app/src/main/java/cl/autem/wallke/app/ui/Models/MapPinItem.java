package cl.autem.wallke.app.ui.Models;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Luis on 11-02-2015.
 */
public class MapPinItem implements ClusterItem {
    private final LatLng mPosition;
    public String title;
    public String category;
    public String id;
    public MapPinItem(double lat, double lng, String title,String category, String id) {
        this.title = title;
        this.id = id;
        this.category = category;
        mPosition = new LatLng(lat, lng);
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }
}
