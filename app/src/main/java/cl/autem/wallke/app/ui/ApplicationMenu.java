package cl.autem.wallke.app.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import cl.autem.wallke.R;

/**
 * Created by claudiocarvajal on 27-05-14.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ApplicationMenu extends Activity implements SearchView.OnQueryTextListener {

    private SearchView mSearchView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.application_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.app_menu);
        mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setQueryHint("Search...");
        mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Toast.makeText(this, "Searching for " + newText, Toast.LENGTH_LONG).show();

        return false;
    }
}
