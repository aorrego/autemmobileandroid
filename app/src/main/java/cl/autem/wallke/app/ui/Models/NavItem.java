package cl.autem.wallke.app.ui.Models;

/**
 * Created by Luis on 03-02-2015.
 */
public class NavItem {
    public String mTitle;
    public int mIcon;

    public NavItem(String title, int icon) {
        mTitle = title;
        mIcon = icon;
    }
}
