package cl.autem.wallke.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import cl.autem.wallke.R;
import cl.autem.wallke.services.event.Event;
import cl.autem.wallke.services.event.EventServices;
import cl.autem.wallke.services.event.IEventServices;
import cl.autem.wallke.services.maps.AutemApi;


public class NewEventMap extends Activity implements GoogleMap.OnMapClickListener, View.OnClickListener {

    private GoogleMap googleMap;
    private IEventServices eventServices = new EventServices();

    LatLng latLng = new LatLng(-33.43 , -70.62);
    MarkerOptions markerOptions;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event_map);
       // latLng = new LatLng(googleMap.getMyLocation().getLatitude(), googleMap.getMyLocation().getLongitude());
        View btnSearch = findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
        try
        {
            if(googleMap == null)
            {
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            }

            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setCompassEnabled(true);
            Marker tp = googleMap.addMarker(new MarkerOptions().position(latLng).title("Prueba"));
            googleMap.setOnMapClickListener(this);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
       // new HttpRequestTask().execute();

        if(v.getId() == findViewById(R.id.btnSearch).getId())
        {
            TextView txtAddress = (TextView)findViewById(R.id.txtAddress);
            String Address = txtAddress.getText().toString();
            if(Address != null && !Address.equals(""))
            {
                new AddressTask().execute(Address);
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.application_menu, menu);
        return true;
    }



    public void animateCamera(View view) {
        if (googleMap.getMyLocation() != null)
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(googleMap.getMyLocation().getLatitude(), googleMap.getMyLocation().getLongitude()), 15));
    }

    @Override
    public void onMapClick(LatLng latLng) {
        String s = String.valueOf(latLng);
        Geocoder geocoder = new Geocoder(getBaseContext());
        List<Address> addresses = null;
        String addressText="";
        try
        {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude,1);
            if(addresses != null && addresses.size() > 0 )
            {
                Address address = addresses.get(0);
                addressText = String.format("%s,%s", address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "", address.getCountryName());

                TextView txtAddress = (TextView)findViewById(R.id.txtAddress);

                txtAddress.setText(addressText);

            }

        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        googleMap.clear();
        googleMap.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                .draggable(true)
                .title("Prueba")
                .snippet(String.valueOf(latLng))
                .flat(true)

        ).showInfoWindow();

        eventServices.createEvent(latLng.latitude, latLng.longitude, 4);
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, AutemApi> {
        @Override
        protected AutemApi doInBackground(Void... params)
        {
            String result = "";
            Event[] events = null;
            ResponseEntity<Event[]> responseEntity = null;
            RestTemplate restTemplate = new RestTemplate();
            try
            {
                final String url = "http://autem.azurewebsites.net/api/events";
                List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
                messageConverters.add(new MappingJackson2HttpMessageConverter());

                events = restTemplate.getForObject(url, Event[].class);

                return null;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

            return null;
        }

        private HttpEntity<?> getRequestEntity() {
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setAccept(Collections.singletonList(new MediaType("application", "json")));
            return new HttpEntity<Object>(requestHeaders);
        }

        @Override
        protected void onPostExecute(AutemApi autemApi)
        {
           /* String lat = autemApi.getLat();
            String ln = autemApi.getLn();
            String rad = autemApi.getRad();*/
        }
    }


    private class AddressTask extends AsyncTask<String, Void, List<Address>> {
        @Override
        protected List<Address> doInBackground(String... locationName) {
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocationName(locationName[0], 3);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            return addresses;
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {
            if (addresses == null || addresses.size() == 0) {
                Toast.makeText(getBaseContext(), "No se encontró dirección", Toast.LENGTH_LONG).show();
            }

            googleMap.clear();

            for (int i = 0; i < addresses.size(); i++) {
                Address address = (Address) addresses.get(i);

                latLng = new LatLng(address.getLatitude(), address.getLongitude());
                String addressText = String.format("%s,%s", address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "", address.getCountryName());

                markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(addressText);

                googleMap.addMarker(markerOptions);

                if (i == 0)
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        }

     }
 }
