package cl.autem.wallke.app.tasks;

import android.os.AsyncTask;

import cl.autem.wallke.app.MainMapActivity;
import cl.autem.wallke.app.WallkeApplication;
import cl.autem.wallke.app.ui.HomeMapViewManager;
import cl.autem.wallke.location.WallkeLocationManager;
import cl.autem.wallke.services.place.IPlaceManager;
import cl.autem.wallke.services.place.Place;
import cl.autem.wallke.services.place.PlaceManager;


/**
 * Created by Alejandro on 31-03-2015.
 */
public class GetPlacesTask extends AsyncTask<Void, Void, Void>  {

    private final String apiUrl = "http://autemapi.azurewebsites.net/"; // TODO: Cambiar por algun sistema de configuración
    private final android.location.Location loc;
    private final MainMapActivity parentActivity;
    private final String[] categories;
    private final int zoomLevel;
    private IPlaceManager placeManager;
    private WallkeLocationManager wallkeLocationManager;
    private HomeMapViewManager homeMapViewManager;

    public GetPlacesTask(MainMapActivity parentActivity, android.location.Location loc, String[] categories, int zoomLevel, WallkeLocationManager wallkeLocationManager, HomeMapViewManager homeMapViewManager){
        this.loc = loc;
        this.parentActivity = parentActivity;
        this.categories = categories;
        this.zoomLevel = zoomLevel;
        this.wallkeLocationManager = wallkeLocationManager;
        placeManager = new PlaceManager();
        this.homeMapViewManager = homeMapViewManager;
    }

    @Override
    protected Void doInBackground(Void... params) {

        final Place[] places = placeManager.getHomePlaces(loc, categories, zoomLevel);
        ((WallkeApplication) parentActivity.getApplication()).setPlaces(places);
        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                wallkeLocationManager.createCluster(places);
            }
        });

        homeMapViewManager.hideProgressDialog();
        // ((WallkeApplication) parentActivity.getApplication()).setPlaces(placeManager.getHomePlaces(loc, categories, zoomLevel));
       // wallkeLocationManager.createCluster(((WallkeApplication) parentActivity.getApplication()).getPlaces());

        return null;
    }
}
