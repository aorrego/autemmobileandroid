package cl.autem.wallke.app.messaging;

import android.support.v7.app.ActionBarActivity;

import cl.autem.wallke.R;
import cl.autem.wallke.services.communication.WSFactory;

public class CommunicationManager {

    private WSFactory wsFactory;
    private ActionBarActivity parentActivity;

    public CommunicationManager(ActionBarActivity parentActivity){
        this.parentActivity = parentActivity;
        wsFactory = new WSFactory(parentActivity,parentActivity.getApplicationContext() );
        wsFactory.initWebsocket( parentActivity.getString(R.string.urlWSServer));

    }

    public void sendMessage(String message){
        wsFactory.sendMessage(message);
    }
}
