package cl.autem.wallke.app;

import android.app.Application;
import android.app.ProgressDialog;

import cl.autem.wallke.services.place.Place;

public class WallkeApplication extends Application {

    private Place[] places;


    public Place[] getPlaces() {
        return places;
    }

    public void setPlaces(Place[] places) {
        this.places = places;
    }
}