package cl.autem.wallke.app;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;

import cl.autem.wallke.R;
import cl.autem.wallke.app.geofencing.GeofenceManager;
import cl.autem.wallke.app.messaging.CommunicationManager;
import cl.autem.wallke.app.ui.HomeMapViewManager;
import cl.autem.wallke.location.WallkeLocationManager;
import cl.autem.wallke.services.place.IPlaceManager;
import cl.autem.wallke.services.place.PlaceManager;

public class MainMapActivity  extends ActionBarActivity
        implements ConnectionCallbacks, OnConnectionFailedListener {

    protected static final String TAG = "cre-and-mon-geofences";

    // Managers
    private WallkeLocationManager wallkeLocationManager;
    private HomeMapViewManager homeMapViewManager;
    private IPlaceManager placeManager;
    private CommunicationManager communicationManager;
    private GeofenceManager geofenceManager;

    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;
    private GoogleApiClient googleApiClient;

   /* protected PlacesModule placesModule;
    private HomeComponent component;

    HomeComponent component() {
        if (component == null) {
            component = Dagger_HomeComponent.builder()
                    .applicationComponent(((WallkeApp) getApplication()).component())
                    .activityModule(new ActivityModule(this))
                    .build();
        }
        return component;
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //placesModule = new PlacesModule();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_map);

        initGooglePlayServices();
        communicationManager = new CommunicationManager(this);

        placeManager = new PlaceManager();
        homeMapViewManager = new HomeMapViewManager(this, communicationManager, placeManager  );
        homeMapViewManager.initialize();

        homeMapViewManager.showProgressDialog();

        geofenceManager = new GeofenceManager(this, wallkeLocationManager, googleApiClient);
        wallkeLocationManager = new WallkeLocationManager(this, placeManager, homeMapViewManager, geofenceManager, googleApiClient);
        wallkeLocationManager.initialize();

        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean( WallkeConstants.STATE_RESOLVING_ERROR, false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {  // more about this later
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(WallkeConstants.STATE_RESOLVING_ERROR, mResolvingError);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == WallkeConstants.REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!googleApiClient.isConnecting() &&
                        !googleApiClient.isConnected()) {
                    googleApiClient.connect();
                }
            }
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        //initPlaces();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, WallkeConstants.REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                googleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            homeMapViewManager.showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    private void initGooglePlayServices() {
        // Create a GoogleApiClient instance
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
    }
}