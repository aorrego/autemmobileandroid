package cl.autem.wallke.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Toast;

import cl.autem.wallke.R;
import cl.autem.wallke.twitter.Constants;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LoginActivity extends Activity implements SearchView.OnQueryTextListener {

    private static Twitter twitter;
    private static SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;
    private static RequestToken requestToken;
    private WebView webView;
    private ImageButton btnLoginTwitter;
    private ImageButton btnLoginFacebook;

    private SearchView mSearchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mSharedPreferences = getApplicationContext().getSharedPreferences("Usuario", 0);
        verificarAutenticacionTwitter();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inicializarLogin();

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


       // editor = mSharedPreferences.edit();
        //editor.putBoolean("isTwitterLogedIn", false);
        //editor.commit();

        try {
            if (!isTwitterLoggedInAlready()) {
                Uri uri = this.getIntent().getData();
                if (uri != null && uri.toString().startsWith(Constants.TWITTER_CALLBACK_URL)) {
                    // oAuth verifier
                    String verifier = uri.getQueryParameter(Constants.URL_TWITTER_OAUTH_VERIFIER);
                    try
                    {
                        AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
                        editor = mSharedPreferences.edit();
                        editor.putString(Constants.PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
                        editor.putString(Constants.PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
                        editor.putBoolean(Constants.PREF_KEY_TWITTER_LOGIN,true);
                        editor.commit();

                        long UserId = accessToken.getUserId();
                        User user = twitter.showUser(UserId);
                        String userName = user.getName();
                        String userDescription = user.getDescription();
                        String userImage = user.getOriginalProfileImageURL();

                        editor = mSharedPreferences.edit();
                        editor.putString("userImageUrl", userImage);
                        editor.putBoolean("isTwitterLogedIn", true);
                        editor.commit();

                        startActivity(new Intent().setClass(this, MainMapActivity.class));
                    }
                    catch (Exception ex)
                    {
                        Log.e("Twitter Login Error", "> " + ex.getMessage());
                    }
                }
            }
        }
        catch (Exception e)
        {
            // Check log for login errors
            Log.e("Twitter Login Error", "> " + e.getMessage());
        }
    }

    private void verificarAutenticacionTwitter() {
        if (mSharedPreferences.getBoolean("isTwitterLogedIn", false)  ){
            startActivity(new Intent().setClass(this, MainMapActivity.class));
        }
    }

    private void inicializarLogin() {
        btnLoginTwitter = (ImageButton) findViewById(R.id.btnLoginTwitter);
        btnLoginFacebook = (ImageButton) findViewById(R.id.btnLoginFacebook);
        btnLoginTwitter.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                loginToTwitter();
            }

          /*   public void onClick(View v) {
                startActivity(new Intent().setClass(v.getContext(), OAuthAccessTokenActivity.class)); //Cuando se arregle Twitter se debe descomentar
                //startActivity(new Intent().setClass(v.getContext(), MainMapActivity.class));
            }*/
        });
    }

    public void loginToTwitter() {
        // Check if already logged in
        if(!isTwitterLoggedInAlready())
        {
            btnLoginTwitter.setVisibility(View.GONE);
            btnLoginFacebook.setVisibility(View.GONE);

            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(Constants.CONSUMER_KEY);
            builder.setOAuthConsumerSecret(Constants.CONSUMER_SECRET);
            Configuration configuration = builder.build();

            TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                requestToken = twitter.getOAuthRequestToken(Constants.TWITTER_CALLBACK_URL);

                webView = (WebView) findViewById(R.id.webView);

                WebSettings webSettings = webView.getSettings();
                webSettings.setJavaScriptEnabled(true);

                webView.loadUrl(requestToken.getAuthenticationURL());
                //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL()));
                //browser.getContext().startActivity(intent);

               // this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL())));
            }
            catch (TwitterException e) {
                e.printStackTrace();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Conectado a Twitter", Toast.LENGTH_LONG).show();
        }
    }

    private static boolean isTwitterLoggedInAlready() {

        // return twitter login status from Shared Preferences
        return  mSharedPreferences.getBoolean("isTwitterLogedIn",false);
    }
    private static void logoutFromTwitter()
    {
        try
        {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.remove(Constants.ACCESS_TOKEN);
            editor.remove(Constants.ACCESS_TOKEN_SECRET);
            editor.remove("isTwitterLogedIn");
            editor.commit();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       try
       {
           // Inflate the menu; this adds items to the action bar if it is present.
           getMenuInflater().inflate(R.menu.application_menu, menu);

           MenuItem searchItem = menu.findItem(R.id.app_menu);
           mSearchView = (SearchView) searchItem.getActionView();
           mSearchView.setQueryHint("Buscar...");
           mSearchView.setOnQueryTextListener(this);
       }
       catch (Exception ex)
       {
           ex.printStackTrace();
       }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

       // Toast.makeText(this, "Searching for " + newText, Toast.LENGTH_LONG).show();

        return false;
    }
}
