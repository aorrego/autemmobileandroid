package cl.autem.wallke.app.ui.dialog;

import android.app.Dialog;
import android.app.Notification;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;

import com.google.android.gms.common.GooglePlayServicesUtil;

import cl.autem.wallke.app.MainMapActivity;
import cl.autem.wallke.app.WallkeConstants;

/* A fragment to display an error dialog */
public class ErrorDialogFragment extends DialogFragment {

    private ActionBarActivity parentActivity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Get the error code and retrieve the appropriate dialog
        int errorCode = this.getArguments().getInt(WallkeConstants.DIALOG_ERROR);
        return GooglePlayServicesUtil.getErrorDialog(errorCode,
                this.getActivity(), WallkeConstants.REQUEST_RESOLVE_ERROR);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
       ((MainMapActivity)getActivity()).onDialogDismissed();
    }

    /* Creates a dialog for an error message */
    public void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(WallkeConstants.DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show( ((MainMapActivity)getActivity()).getSupportFragmentManager(), "errordialog");
    }
}