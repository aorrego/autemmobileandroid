package cl.autem.wallke.app;

/**
 * Created by Alejandro on 07-04-2015.
 */
public class WallkeConstants {
    // Unique tag for the error dialog fragment
    public static final String DIALOG_ERROR = "dialog_error";
    // Request code to use when launching the resolution activity
    public static final int REQUEST_RESOLVE_ERROR = 1001;

    public static final String STATE_RESOLVING_ERROR = "resolving_error";
}
