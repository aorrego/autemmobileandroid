package cl.autem.wallke.app.ui.Renders;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import cl.autem.wallke.app.ui.Models.MapPinItem;

/**
 * Created by Luis on 11-02-2015.
 */
public class MapClusterRender<T extends MapPinItem> extends DefaultClusterRenderer<T> {

    private IconGenerator mClusterIconGenerator;
    private ImageView mClusterImageView;
    private Context context;

    public MapClusterRender(Context context, GoogleMap map, ClusterManager<T> clusterManager) {
        super(context, map, clusterManager);
        mClusterIconGenerator = new IconGenerator(context);
        mClusterIconGenerator.setColor(Color.GREEN);

    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<T> cluster) {
        //start clustering if at least 2 items overlap
        return cluster.getSize() > 1;
    }

    @Override
    protected void onBeforeClusterItemRendered(MapPinItem item, MarkerOptions markerOptions) {
        markerOptions.title(item.title);
    }


}
