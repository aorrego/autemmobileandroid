package cl.autem.wallke.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;

import cl.autem.wallke.R;


/**
 * Created by claudiocarvajal on 08-05-14.
 */
public class NewEventActivity extends Activity implements View.OnClickListener, TimePickerFragment.Listener {

    private DatePickerDialog datePickerDialogEventStart;
    private DatePickerDialog datePickerDialogEventEnd;

    private TextView textViewEventStart;
    private TextView textViewEventEnd;
    private TextView textViewEventStartTime;
    private TextView textViewEventEndTime;
    private boolean  flag = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        TimePickerFragment.setListener(this);

        findViewsById();
        setCalendarEvent();
    }

    private void findViewsById()
    {
        textViewEventStart = (TextView) findViewById(R.id.tvEventStartDay);
        textViewEventStart.setInputType(InputType.TYPE_NULL);
        textViewEventStart.requestFocus();

        textViewEventEnd = (TextView) findViewById(R.id.tvEventEndDay);
        textViewEventEnd.setInputType(InputType.TYPE_NULL);

        textViewEventStartTime = (TextView) findViewById(R.id.tvEventStartTime);
        textViewEventStartTime.setInputType(InputType.TYPE_NULL);

        textViewEventEndTime = (TextView) findViewById(R.id.tvEventEndTime);
        textViewEventEndTime.setInputType(InputType.TYPE_NULL);
    }

    private void setCalendarEvent()
    {
        textViewEventStart.setOnClickListener(this);
        textViewEventEnd.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();

        datePickerDialogEventStart = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year,monthOfYear,dayOfMonth);
                String formatDate = DateFormat.getDateInstance(DateFormat.SHORT).format(newDate.getTime());
                textViewEventStart.setText(formatDate);
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialogEventEnd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year,monthOfYear,dayOfMonth);
                String formatDate = DateFormat.getDateInstance(DateFormat.SHORT).format(newDate.getTime());
                textViewEventEnd.setText(formatDate);
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


    @Override
    public void onClick(View v) {
        if(v == textViewEventStart)
        {
            datePickerDialogEventStart.show();
        }
        else if(v == textViewEventEnd)
        {
            datePickerDialogEventEnd.show();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void ShowTimePickerDialogTimeStart(View v)
    {
        flag = false;
        DialogFragment dialogFragment = new TimePickerFragment();
        dialogFragment.show(getFragmentManager(),"");
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void ShowTimePickerDialogTimeEnd(View v)
    {
        flag = true;
        DialogFragment dialogFragment = new TimePickerFragment();
        dialogFragment.show(getFragmentManager(),"");
    }

    @Override
    public void setTime(int hourOfDay, int minute) {
        String strMinute =  ((minute < 10) ? ("0" + minute) : String.valueOf(minute));
        String timeString = hourOfDay + ":" + strMinute;
        if(flag == false)
            textViewEventStartTime.setText(timeString);
        else
            textViewEventEndTime.setText(timeString);
    }
}
