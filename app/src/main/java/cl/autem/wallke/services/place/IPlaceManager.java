package cl.autem.wallke.services.place;

import android.graphics.drawable.Drawable;

/**
 * Created by Developer on 11/06/2014.
 */
public interface IPlaceManager {
    public Place[] getHomePlaces(android.location.Location loc, String[] category,int zoomLevel); // Temporary get method

    public void createPlace(Double lat, Double ln, Integer rad);

    public Drawable getImage(Place place);
}
