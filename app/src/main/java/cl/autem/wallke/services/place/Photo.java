package cl.autem.wallke.services.place;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Luis on 13-02-2015.
 */
public class Photo {

    @JsonProperty
    public int width;

    @JsonProperty
    public int height;

    @JsonProperty
    public String url;

    @JsonProperty
    public String idFoursquare;
}
