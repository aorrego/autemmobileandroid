package cl.autem.wallke.services.place;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Luis on 13-02-2015.
 */
public class Contact {
    @JsonProperty
    private String website;

    @JsonProperty
    private String phone;

    @JsonProperty
    private Social social;

}
