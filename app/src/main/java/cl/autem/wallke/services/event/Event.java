package cl.autem.wallke.services.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Event {

    @JsonProperty
    private String name;

    @JsonProperty
    private String address;

    @JsonProperty
    private String _id;

    @JsonProperty
    private double lat;

    @JsonProperty
    private double lng;

    @JsonProperty
    private String rad;

    public String getName() { return name; }

    public String getAddress() { return address; }

    public double getLat() {
        return this.lat;
    }

    public double getLng() {
        return this.lng;
    }

    public String getRad() {
        return this.rad;
    }

    public String get_Id() { return _id; }
}
