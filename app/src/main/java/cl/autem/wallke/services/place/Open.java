package cl.autem.wallke.services.place;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Luis on 16-02-2015.
 */
public class Open {
    @JsonProperty
    private String end;

    @JsonProperty
    private String start;
}
