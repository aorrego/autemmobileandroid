package cl.autem.wallke.services.place;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Luis on 13-02-2015.
 */
public class Location {

    @JsonProperty
    public Double[] loc;

    @JsonProperty
    public String address;

    @JsonProperty
    public String city;

    @JsonProperty
    public String country;

    @JsonProperty
    public String postalCode;

    @JsonProperty
    public String state;
}
