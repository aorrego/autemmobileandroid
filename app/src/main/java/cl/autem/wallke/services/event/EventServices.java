package cl.autem.wallke.services.event;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class EventServices implements IEventServices {

    final String urlGetAllEvents = "http://autem.azurewebsites.net/api/events"; // TODO: Cambiar por algun sistema de configuración
    final String urlPostEvent = "http://autem.azurewebsites.net/api/events";
    RestTemplate restTemplate = new RestTemplate();

    @Override
    public Event[] getAllEvents() {

        Event[] events = null;
        try
        {
            List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
            messageConverters.add(new MappingJackson2HttpMessageConverter());
            events = restTemplate.getForObject(urlGetAllEvents, Event[].class);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return events;
    }

    public void createEvent(EventCreateModel model){

    }

    @Override
    public void createEvent(Double lat, Double ln, Integer rad) {

        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<String, String>();
        mvm.add("lat", lat.toString());
        mvm.add("ln", ln.toString());
        mvm.add("rad", rad.toString());
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new FormHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
        String result = restTemplate.postForObject(urlPostEvent, mvm, String.class);
    }
}
