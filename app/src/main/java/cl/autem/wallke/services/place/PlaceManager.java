package cl.autem.wallke.services.place;


import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.util.Log;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class PlaceManager implements IPlaceManager {

    final String apiUrl = "http://autemapi.azurewebsites.net/"; // TODO: Cambiar por algun sistema de configuración
    RestTemplate restTemplate = new RestTemplate();

    public static final String TAG = PlaceManager.class.getSimpleName();

    @Override
    public Place[] getHomePlaces(android.location.Location loc, String[] category, int zoomLevel) {

        Place[] place = null;
        try
        {
            URI uri = UriComponentsBuilder.fromUriString(apiUrl)
                    .path("places")
                    .queryParam("lat", loc.getLatitude())
                    .queryParam("lng", loc.getLongitude())
                    .queryParam("distance", 2000)
                    .queryParam("limit", 99)
                    .build()
                    .toUri();
            List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
            messageConverters.add(new MappingJackson2HttpMessageConverter());
            place = restTemplate.getForObject(uri , Place[].class);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return place;
    }

    public void createPlace(Double lat, Double ln, Integer rad) {

        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<String, String>();
        mvm.add("lat", lat.toString());
        mvm.add("ln", ln.toString());
        mvm.add("rad", rad.toString());
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new FormHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
        String result = restTemplate.postForObject(apiUrl, mvm, String.class);
    }

    public Drawable getImage(Place place){
        try {
            if (place.photos.length != 0) {
                String imgUrl = place.photos[0].url.replace("[SIZE]","400x400");
                Log.d("INFO", "img URL " + imgUrl);
                InputStream is = (InputStream) new URL(imgUrl).getContent();
                if(is != null){
                    Drawable img = Drawable.createFromStream(is, "foursquare");
                    return img;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }catch (HttpMessageNotReadableException e) {
            e.printStackTrace();
        }
        return new Drawable() {
            @Override
            public void draw(Canvas canvas) {

            }

            @Override
            public void setAlpha(int alpha) {

            }

            @Override
            public void setColorFilter(ColorFilter cf) {

            }

            @Override
            public int getOpacity() {
                return 0;
            }
        };
    }
}
