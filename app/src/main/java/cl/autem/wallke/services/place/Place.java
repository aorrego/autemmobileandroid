package cl.autem.wallke.services.place;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Place {

    @JsonProperty
    public String _id;

    @JsonProperty
    public String name;

    @JsonProperty
    public String description;

    @JsonProperty
    public Categories[] categories;

    @JsonProperty
    public Contact contact;

    @JsonProperty
    public Location location;

    @JsonProperty
    public Schedule schedule;

    @JsonProperty
    public String[] payment;

    @JsonProperty
    public Photo[] photos;

}
