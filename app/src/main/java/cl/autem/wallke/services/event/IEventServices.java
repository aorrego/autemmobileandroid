package cl.autem.wallke.services.event;

/**
 * Created by Developer on 11/06/2014.
 */
public interface IEventServices {
    public Event[] getAllEvents(); // Temporary get method

    public void createEvent(EventCreateModel model);

    public void createEvent(Double lat, Double ln, Integer rad);
}
