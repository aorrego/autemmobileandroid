package cl.autem.wallke.services.place;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Luis on 13-02-2015.
 */
public class Social {

    @JsonProperty
    private String facebook;

    @JsonProperty
    private String twitter;

    @JsonProperty
    private String instagram;
}
