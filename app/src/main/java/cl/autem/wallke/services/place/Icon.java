package cl.autem.wallke.services.place;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Luis on 13-02-2015.
 */
public class Icon {

    @JsonProperty
    public String sufix;

    @JsonProperty
    public String prefix;
}
