package cl.autem.wallke.services.maps;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by claudiocarvajal on 09-06-14.
 */
public class AutemApi {

    @JsonIgnore()
    private String _id;
    private String lat;
    private String ln;
    private String rad;

    public String getLat() {
        return this.lat;
    }

    public String getLn() {
        return this.ln;
    }

    public String getRad() {
        return this.rad;
    }

    public String get_Id() { return _id; }
}
