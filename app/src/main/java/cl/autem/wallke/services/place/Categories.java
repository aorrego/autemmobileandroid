package cl.autem.wallke.services.place;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Luis on 13-02-2015.
 */
public class Categories {

    @JsonProperty
    public Icon icon;

    @JsonProperty
    public String shortName;

    @JsonProperty
    public Boolean primary;

    @JsonProperty
    public String pluralName;

    @JsonProperty
    public String name;

    @JsonProperty
    public String _id;
}
