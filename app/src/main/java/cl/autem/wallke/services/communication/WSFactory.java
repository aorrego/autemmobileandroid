package cl.autem.wallke.services.communication;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

/**
 * Created by Alejandro on 19-03-2015.
 */
public class WSFactory {

    private Context applicationContext;
    private FragmentActivity parentActivity;

    public WSFactory(FragmentActivity activity, Context applicationContext) {
        this.applicationContext = applicationContext;
        this.parentActivity = activity;

    }
    private Socket webSocket;

    public Socket getWebSocket()
    {
        if ( webSocket == null )
            throw new ExceptionInInitializerError("No se ha inicializado el websocket.");
        return webSocket;
    }

    public void initWebsocket(String url){
        try {
            webSocket = IO.socket(url);
            webSocket.on("client", new Emitter.Listener() {

                @Override
                public void call(Object... args) {

                    try  {
                    final String broadcastMessage = args[0].toString();

                        parentActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(parentActivity, broadcastMessage, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    catch ( Exception ex){
                        Object a = ex;
                    }
                }
            });
            webSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    //Object asd = args;
                    //webSocket.emit("messages", "connecting...");
                    //webSocket.disconnect();
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                }

            });
            //webSocket.connect();
        }
        catch (URISyntaxException e) {}
    }

    public void sendMessage(String message)
    {
        webSocket.connect();
        webSocket.emit("messages", message );
    }
}
