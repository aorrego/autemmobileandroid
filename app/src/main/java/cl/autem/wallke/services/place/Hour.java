package cl.autem.wallke.services.place;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Luis on 16-02-2015.
 */
public class Hour {
    @JsonProperty
    private int[] days;

    @JsonProperty
    private boolean includesToday;

    @JsonProperty
    private Open[] open;
}
